//
//  DetailViewModelTest.swift
//  Airports
//
//  Created by rhomhazar on 7/26/21.
//

import XCTest
@testable import Airports

class DetailViewModelTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testGetName() throws {
        let airport = Airport.init(code: "code", name: "testName", isInternational: false, isDomestic: false, isRegional: false, isOnline: false, isEticketable: false, location: Location.init(latitude: 0.0, latitudeRadius: 0.0, longitude: 0.0, longitudeRadius: 0.0), city: City.init(code: "cityCode", timeZoneName: "cityTimeZone"), country: Country.init(code: "countryCode", name: "countryName"), region: Region.init(code: "regionName", name: "regionString"))
        
        let viewModel = DetailViewModel.init(airport: airport)
        
        XCTAssert(viewModel.getName() == "testName")
    }
    
    func testGetCode() throws {
        let airport = Airport.init(code: "code", name: "testName", isInternational: false, isDomestic: false, isRegional: false, isOnline: false, isEticketable: false, location: Location.init(latitude: 0.0, latitudeRadius: 0.0, longitude: 0.0, longitudeRadius: 0.0), city: City.init(code: "cityCode", timeZoneName: "cityTimeZone"), country: Country.init(code: "countryCode", name: "countryName"), region: Region.init(code: "regionName", name: "regionString"))
        
        let viewModel = DetailViewModel.init(airport: airport)
        
        XCTAssert(viewModel.getCode() == "code")
    }
    
    func testGetCountry() throws {
        let airport = Airport.init(code: "code", name: "testName", isInternational: false, isDomestic: false, isRegional: false, isOnline: false, isEticketable: false, location: Location.init(latitude: 0.0, latitudeRadius: 0.0, longitude: 0.0, longitudeRadius: 0.0), city: City.init(code: "cityCode", timeZoneName: "cityTimeZone"), country: Country.init(code: "countryCode", name: "countryName"), region: Region.init(code: "regionName", name: "regionString"))
        
        let viewModel = DetailViewModel.init(airport: airport)
        
        XCTAssert(viewModel.getCountry() == "countryName (countryCode)")
    }
    
    func testGetCity() throws {
        let airport = Airport.init(code: "code", name: "testName", isInternational: false, isDomestic: false, isRegional: false, isOnline: false, isEticketable: false, location: Location.init(latitude: 0.0, latitudeRadius: 0.0, longitude: 0.0, longitudeRadius: 0.0), city: City.init(code: "cityCode", timeZoneName: "cityTimeZone"), country: Country.init(code: "countryCode", name: "countryName"), region: Region.init(code: "regionName", name: "regionString"))
        
        let viewModel = DetailViewModel.init(airport: airport)
        
        XCTAssert(viewModel.getCity() == "cityCode cityTimeZone")
    }
    
    func testGetRegion() throws {
        let airport = Airport.init(code: "code", name: "testName", isInternational: false, isDomestic: false, isRegional: false, isOnline: false, isEticketable: false, location: Location.init(latitude: 0.0, latitudeRadius: 0.0, longitude: 0.0, longitudeRadius: 0.0), city: City.init(code: "cityCode", timeZoneName: "cityTimeZone"), country: Country.init(code: "countryCode", name: "countryName"), region: Region.init(code: "regionName", name: "regionString"))
        
        let viewModel = DetailViewModel.init(airport: airport)
        
        XCTAssert(viewModel.getRegion() == "regionString (regionName)")
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
