//
//  Airport.swift
//  MasterDetail_MVVM
//
//  Created by rhomhazar on 7/25/21.
//

struct Location: Decodable {
//    let seaLevel: CLong
    let latitude: Float
    let latitudeRadius: Float
    let longitude: Float
    let longitudeRadius: Float
//    let latitudeDir: String
//    let longitudeDir: String
    
    enum CodingKeys: String, CodingKey  {
//        case seaLevel = "aboveSeaLevel"
        case latitude
        case latitudeRadius
        case longitude
        case longitudeRadius
//        case latitudeDir = "latitudeDirection"
//        case longitudeDir = "longitudeDirection"
    }
    
}

struct City: Decodable {
    let code: String
//    let name: String
    let timeZoneName: String
    
    enum CodingKeys: String, CodingKey  {
        case code = "cityCode"
//        case name = "cityName"
        case timeZoneName
    }
}

struct Country: Decodable {
    let code: String
    let name: String
    
    enum CodingKeys: String, CodingKey  {
        case code = "countryCode"
        case name = "countryName"
    }
}

struct Region: Decodable {
    let code: String
    let name: String
    
    enum CodingKeys: String, CodingKey  {
        case code = "regionCode"
        case name = "regionName"
    }
}

struct Airport: Decodable{
    
    let code: String
    let name: String
    let isInternational: Bool
    let isDomestic: Bool
    let isRegional: Bool
    let isOnline: Bool
    let isEticketable: Bool
    let location: Location
    let city: City
    let country: Country
    let region: Region
    
    enum CodingKeys: String, CodingKey  {
        case code = "airportCode"
        case name = "airportName"
        case location
        case isInternational = "internationalAirport"
        case isDomestic = "domesticAirport"
        case isRegional = "regionalAirport"
        case isOnline = "onlineIndicator"
        case isEticketable = "eticketableAirport"
        case city
        case country
        case region
    }
    
}
