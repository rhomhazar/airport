//
//  DetailViewController.swift
//  MasterDetail_MVVM
//
//  Created by rhomhazar on 7/25/21.
//

import UIKit

class DetailViewController: UIViewController, TableViewDelegate{

    @IBOutlet weak var nameLbl: UILabel?
    @IBOutlet weak var codeLbl: UILabel?
    @IBOutlet weak var countryLbl: UILabel?
    @IBOutlet weak var cityLbl: UILabel?
    @IBOutlet weak var regionLbl: UILabel?

    
    var viewModel: DetailViewModel?{
        willSet(newViewModel) {
            guard let vm = newViewModel else {
                self.nameLbl?.text = ""
                self.codeLbl?.text = ""
                self.countryLbl?.text = ""
                self.cityLbl?.text = ""
                self.regionLbl?.text = ""
                return
            }
            self.nameLbl?.text = vm.getName()
            self.codeLbl?.text = vm.getCode()
            self.countryLbl?.text = vm.getCountry()
            self.cityLbl?.text = vm.getCity()
            self.regionLbl?.text = vm.getRegion()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameLbl?.text = viewModel?.getName()
        self.codeLbl?.text = viewModel?.getCode()
        self.countryLbl?.text = viewModel?.getCountry()
        self.cityLbl?.text = viewModel?.getCity()
        self.regionLbl?.text = viewModel?.getRegion()
    }


}

