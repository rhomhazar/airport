//
//  TableViewController.swift
//  MasterDetail_MVVM
//
//  Created by rhomhazar on 7/25/21.
//

import UIKit

protocol TableViewDelegate: class{
    
}

class AirportTableViewController: UITableViewController, TableViewModelDelegate {
    
    weak var delegate: TableViewDelegate?
    
    private var viewModel: TableViewModel?
    
    private var activityView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = TableViewModel.init()
        self.viewModel?.delegate = self
        
        self.activityView =  setupActivityView()
        
        refreshTable()
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as? AirportCell else {
            return UITableViewCell()
        }
        
        cell.viewModel = viewModel?.cellViewModel(index: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let detailViewController = delegate as? DetailViewController{
            
            detailViewController.viewModel = viewModel?.detailViewModel(index: indexPath.row)
            
            splitViewController?.showDetailViewController(detailViewController, sender: nil)
        }
    }
    

    //TableViewModelDelegate methods
    func reloadTable() {
        self.tableView.reloadData()
        
        showActvityView(shouldShow: false)
    }
    
    func showFetchError() {
        showActvityView(shouldShow: false)
        
        let alert = UIAlertController(title: "Error", message: "Failed to fetch airport data.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: {_ in }))
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: {_ in
            self.refreshTable()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //Refresh
    func refreshTable(){
        self.viewModel?.fetchAirports()
        
        showActvityView(shouldShow: true)
    }
    
    //Activity View
    func setupActivityView() -> UIView{
        guard let frame = splitViewController?.view.frame else {
            return UIView()
        }
        
        let view = UIView.init(frame: frame)
        view.backgroundColor = .black
        view.alpha = 0.5
        
        let activityIndicator = UIActivityIndicatorView.init(frame: CGRect(x: frame.width/2 - 50, y: frame.height/2 - 50, width: 100, height: 100))
        activityIndicator.color = .white
        activityIndicator.style = .large
        view.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        splitViewController?.view.addSubview(view)
        
        return view
    }
    
    func showActvityView(shouldShow: Bool) {
        self.activityView?.isHidden = !shouldShow
    }
    
}

