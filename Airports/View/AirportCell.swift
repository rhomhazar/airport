//
//  TableViewCell.swift
//  MasterDetail_MVVM
//
//  Created by rhomhazar on 7/25/21.
//

import UIKit

class AirportCell: UITableViewCell {
    
    var viewModel: TableViewCellModel?{
        willSet(newViewModel) {
            guard let vm = newViewModel else {
                self.nameLabel?.text = ""
                self.countyLabel?.text = ""
                return
            }
            self.nameLabel?.text = vm.getName()
            self.countyLabel?.text = vm.getCountryName()
        }
    }
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var countyLabel: UILabel?
    

}
