//
//  TableViewModel.swift
//  MasterDetail_MVVM
//
//  Created by rhomhazar on 7/25/21.
//

import Foundation
import Alamofire

protocol TableViewModelDelegate: class{
    func reloadTable()
    func showFetchError()
}

class TableViewModel: NSObject {
    
    weak var delegate: TableViewModelDelegate?
    
    private var airports: [Airport]
    
    override init() {
        self.airports = []
    }
    
    func fetchAirports() {
        AF.request("https://api.qantas.com/flight/refData/airport").responseDecodable(of: [Airport].self) {
            response in
            
            if response.response?.statusCode == 200{
                guard let airports = response.value else {
                    self.delegate?.showFetchError()
                    return
                }
                self.airports = airports
                self.delegate?.reloadTable()
                return
            }
            
            self.delegate?.showFetchError()
      }
    }
    
    func numberOfRows() -> Int {
        return self.airports.count
    }
    
    func cellViewModel(index: Int) -> TableViewCellModel {
        let airport = airports[index]
        
        return TableViewCellModel.init(airport: airport)
    }
    
    func detailViewModel(index: Int) -> DetailViewModel {
        let airport = airports[index]
        
        return DetailViewModel.init(airport: airport)
    }
}
