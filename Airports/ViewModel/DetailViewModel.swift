//
//  DetailViewModel.swift
//  MasterDetail_MVVM
//
//  Created by rhomhazar on 7/26/21.
//

import Foundation

class DetailViewModel: NSObject {
   
    private var airport: Airport
    
    init(airport: Airport) {
        self.airport = airport
    }
    
    func getName() -> String{
        return airport.name
    }
    
    func getCode() -> String{
        return airport.code
    }
    
    func getCountry() -> String{
        return "\(airport.country.name) (\(airport.country.code)) "
    }
    
    func getCity() -> String{
        return "\(airport.city.code) \(airport.city.timeZoneName)"
    }
    
    func getRegion() -> String{
        return "\(airport.region.name) (\(airport.region.code))"
    }
    
}
    
