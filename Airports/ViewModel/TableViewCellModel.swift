//
//  TableViewCellModel.swift
//  MasterDetail_MVVM
//
//  Created by rhomhazar on 7/25/21.
//

import Foundation

class TableViewCellModel: NSObject {
    
    private var airport: Airport
    
    init(airport: Airport) {
        self.airport = airport
    }
    
    func getName() -> String{
        return airport.name
    }
    
    func getCountryName() -> String {
        return airport.country.name
    }
}
